package javari.animal;

public class Aves extends Animal{

    private String specialStat;

    public Aves (Integer id, String type, String name, Gender gender, double length,
                   double weight, String specialStat, Condition condition){
        super(id, type, name, gender, length, weight, condition);

        this.specialStat = specialStat;
    }

    public boolean specificCondition(){
        if (!specialStat.equals("laying eggs")){
            return true;
        }
        return false;
    }

}