package javari.animal;

public class Reptile extends Animal {

    private String specialStat;

    public Reptile(Integer id, String type, String name, Gender gender, double length,
                   double weight, String specialStat, Condition condition) {
        super(id, type, name, gender, length, weight, condition);

        this.specialStat = specialStat;
    }

    public boolean specificCondition() {
        if (specialStat.equals("tame")) {
            return true;
        }
        return false;
    }

}