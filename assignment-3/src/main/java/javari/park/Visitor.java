package javari.park;

import java.util.ArrayList;
import java.util.List;

public class Visitor implements Registration {

    static ArrayList<Visitor> visitors = new ArrayList<>();
    static int counter = 0;

    String name;
    int registID;
    List<SelectedAttraction> selectedAtts = new;

    public Visitor(String name) {
        this.name = name;
        registID += counter;
        visitors.add(this);
    }

    /**
     * Returns the unique ID that associated with visitor's registration
     * in watching an attraction.
     *
     * @return
     */
    public int getRegistrationId() {
        return registID;
    }

    /**
     * Returns the name of visitor that associated with the registration.
     *
     * @return
     */
    public String getVisitorName() {
        return name;
    }

    /**
     * Changes visitor's name in the registration.
     *
     * @param name name of visitor
     * @return
     */
    public String setVisitorName(String name) {
        this.name = name;
        return name;
    }

    /**
     * Returns the list of all attractions that will be watched by the
     * visitor.
     *
     * @return
     */
    public List<SelectedAttraction> getSelectedAttractions() {
        return selectedAtts;
    }

    /**
     * Adds a new attraction that will be watched by the visitor.
     *
     * @param selected the attraction
     * @return {@code true} if the attraction is successfully added into the
     * list, {@code false} otherwise
     */
    public boolean addSelectedAttraction(SelectedAttraction selected) {
        selectedAtts.add(selected);
        return true;
    }

    public Visitor findVisitor(String name) {
        for (Visitor visitor : visitors) {
            if (visitor.getVisitorName().equalsIgnoreCase(name) {
                return visitor;
            }
        }
        return null;
    }
}