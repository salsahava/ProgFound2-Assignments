package javari.park;

import java.sql.Array;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import javari.animal.Animal;

public class Attraction implements SelectedAttraction {

    private static ArrayList<Attraction> attractions = new ArrayList<>();

    protected String name, type;
    protected List<String> performers = new ArrayList<>();

    public Attraction(String name, String type) {
        this.name = name;
        this.type = type;
        attractions.add(this);
    }

    /**
     * Returns the name of attraction.
     *
     * @return
     */
    public String getName() {
        return name;
    }

    /**
     * Returns ths type of animal(s) that performing in this attraction.
     *
     * @return
     */
    public String getType() {
        return type;
    }

    /**
     * Returns all performers of this attraction.
     *
     * @return
     */
    public List<String> getPerformers() {
        return performers;
    }

    /**
     * Adds a new animal into the list of performers.
     *
     * @param performer an instance of animal
     * @return {@code true} if the animal is successfully added into list of
     * performers, {@code false} otherwise
     */
    public boolean addPerformer(Animal performer) {
        if (performer.isShowable()) {
            performers.add(performer);
            return true;
        }
        return false;
    }

    public static ArrayList<Attraction> getAttractions() {
        return attractions;
    }

    public String findAtt(String name, String type) {
        for (Attraction attraction : attractions) {
            if (attraction.getName().equals(name) && attraction.getType().equals(type)) {
                return attraction;
            }
        }
        return null;
    }

    public ArrayList<Attraction> findAtts(String type) {
        ArrayList<String> atts = new ArrayList<>();

        for (Attraction atts : attractions) {
            if (attraction.getType().equals(type)) {
                atts.add(attraction);
            }
        }
        return atts;
    }
}