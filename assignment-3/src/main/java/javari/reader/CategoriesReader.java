package javari.reader;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class CategoriesReader extends CsvReader {

    public CategoriesReader(Path file) throws IOException {
        super(file);
    }

    /**
     * Counts the number of valid records from read CSV file.
     *
     * @return
     */
    public long countValidRecords(){
        int validRec = 0;
    }

    /**
     * Counts the number of invalid records from read CSV file.
     *
     * @return
     */
    public long countInvalidRecords();
}