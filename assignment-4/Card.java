import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.Image;

@SuppressWarnings("serial")
public class Card extends JButton {
    private String id;
    private boolean matched = false;
    private ImageIcon icon;

    public Card(String id) {
        this.id = id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return this.id;
    }

    public void setMatched(boolean matched) {
        this.matched = matched;
    }

    public boolean getMatched() {
        return this.matched;
    }

    public void showIcon(){
        this.setIcon(new ImageIcon(getClass().getResource(String.format("/img/%s.png", id))));
    }
}