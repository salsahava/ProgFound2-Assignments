import java.awt.Dimension;
import javax.swing.JFrame;


public class Game {
    public static void main(String[] args) {
        Board b = new Board();
        b.setLocation(500, 100);
        b.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        b.pack();
        b.setSize(815, 815);
        b.setResizable(false);
        b.setVisible(true);
    }
}
