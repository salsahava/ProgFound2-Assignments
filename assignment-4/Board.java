import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.Timer;
import javax.swing.ImageIcon;
import java.awt.*;
import java.awt.event.*;
import java.awt.Image;
import java.util.ArrayList;
import java.util.List;
import java.util.Collections;

@SuppressWarnings("serial")
public class Board extends JFrame {

    private List<Card> cards;
    private Card selectedCard;
    private Card c1;
    private Card c2;
    private Timer t;
    private static int counter;
    private JLabel label = new JLabel("Number of tries: " + counter);
    List<Card> cardsList = new ArrayList<Card>();

    public Board() {

        int pairs = 18;
        List<Card> cardsList = new ArrayList<Card>();
        List<String> cardIcon = new ArrayList<String>();
        String[] anims = {"Bee", "Cat", "Chick", "Cow", "Dog", "Duck",
                            "Fish", "Ladybug", "Lion", "Monkey", "Parrot", "Penguin",
                            "Pig", "Polarbear", "Seal", "Snake", "Tiger", "Hamster"};

        for (String anim : anims){
            cardIcon.add(anim);
            cardIcon.add(anim);
        }
        Collections.shuffle(cardIcon);

        for (String val : cardIcon) {
            Card c = new Card(val);
            c.setIcon(new ImageIcon(getClass().getResource("/img/Pawprint.png")));
            c.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent ae) {
                    selectedCard = c;
                    doTurn();
                }
            });
            cardsList.add(c);
        }
        this.cards = cardsList;
        //set up the timer
        t = new Timer(750, new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                checkCards();
            }
        });

        t.setRepeats(false);

        setTitle("Remember the Animal");

        createPanel1();
        createPanel2();
    }

    public void createPanel1(){
        JPanel panel1 = new JPanel();
        getContentPane().add(panel1, BorderLayout.CENTER);
        panel1.setLayout(new GridLayout(6, 6));
        for (Card c : cards) {
            panel1.add(c);
        }
    }

    public void createPanel2(){
        JPanel panel2 = new JPanel();
        getContentPane().add(panel2, BorderLayout.PAGE_END);
        panel2.setLayout(new FlowLayout());

        JButton playAgain = new JButton("Play Again?");

        JButton exit = new JButton("Exit");

        panel2.add(playAgain);
        panel2.add(exit);
        panel2.add(label);

        playAgain.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                restart();
            }
        });

        exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                System.exit(0);
            }
        });

    }

    public void restart(){
        label.setText("Number of tries: " + (counter = 0));
        Container contain = getContentPane();
        contain.removeAll();

        JPanel newBoard = new JPanel();
        getContentPane().add(newBoard, BorderLayout.CENTER);
        newBoard.setLayout(new GridLayout(6, 6));
        for (Card c : cards) {
            newBoard.add(c);
        }

        JPanel newBoard2 = new JPanel();
        getContentPane().add(newBoard2, BorderLayout.PAGE_END);
        newBoard2.setLayout(new FlowLayout());

        JButton playAgain = new JButton("Play Again?");

        JButton exit = new JButton("Exit");

        newBoard2.add(playAgain);
        newBoard2.add(exit);
        newBoard2.add(label);

        contain.add(newBoard);
        contain.add(newBoard2);
        validate();
        repaint();

        setVisible(true);

        exit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                System.exit(0);
            }
        });
    }

    public void doTurn() {
        if (c1 == null && c2 == null) {
            c1 = selectedCard;
            c1.showIcon();
        }

        if (c1 != null && c1 != selectedCard && c2 == null) {
            c2 = selectedCard;
            c2.showIcon();
            counter += 1;
            label.setText("Number of tries: " + counter);
            t.start();
        }
    }

    public void checkCards() {
        if (c1.getId() == c2.getId()) {//match condition
            c1.setEnabled(false); //disables the button
            c2.setEnabled(false);
            c1.setVisible(false);
            c2.setVisible(false);
            c1.setMatched(true); //flags the button as having been matched
            c2.setMatched(true);
            if (this.isGameWon()) {
                JOptionPane.showMessageDialog(this, "You win!");
                System.exit(0);
            }
        } else {
            c1.setIcon(new ImageIcon(getClass().getResource("/img/Pawprint.png")));
            c2.setIcon(new ImageIcon(getClass().getResource("/img/Pawprint.png")));
        }
        c1 = null; //reset c1 and c2
        c2 = null;
    }

    public boolean isGameWon() {
        for (Card c : this.cards) {
            if (c.getMatched() == false) {
                return false;
            }
        }
        return true;
    }

}
