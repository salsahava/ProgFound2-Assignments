/*
 Author	: Salsabila Hava Qabita
 NPM	: 1706979461
 Kelas	: C
 GitLab Account	: salsahava
 */

import java.util.Scanner;

public class A1Station {

    private static final double THRESHOLD = 250; // in kilograms

    public static void main(String[] args) {
		// Buat input scanner baru
		Scanner input = new Scanner(System.in);
		
		int numOfCats = Integer.parseInt(input.nextLine());
		
		int counter = 0;
		TrainCar train = null;
		
		for (int i = 0; i < numOfCats; i++)
		{
			String[] inputan = input.nextLine().split(",");
			String nama = inputan[0];
			double weight = Double.parseDouble(inputan[1]);
			double length = Double.parseDouble(inputan[2]);
			
			WildCat cat = new WildCat(nama, weight, length);
			
			if (counter == 0) train = new TrainCar(cat);
			else{
				train = new TrainCar(cat, train);
			} 
			
			counter++;
			
			// Cek apakah total berat dari kereta melebihi threshold
			if (train.computeTotalWeight() > THRESHOLD || i == numOfCats-1)
			{
				System.out.println("The train departs to Javari Park");
				System.out.print("[LOCO]<--");
				train.printCar();
				
				// Menghitung mass index rata-rata
				double avMassIndex = train.computeTotalMassIndex() / counter;
				
				System.out.format("Average mass index of all cats: %.2f\n", avMassIndex);
				
				String category = "";
				
				if (avMassIndex < 18.5) category = "*underweight*";
				else if (avMassIndex >= 18.5 && avMassIndex < 25) category = "*normal*";
				else if (avMassIndex >= 25 && avMassIndex < 30) category = "*overweight*";
				else if (avMassIndex >= 30) category = "*obese*";
				
				System.out.println("In average, the cats in the train are " + category);
				counter = 0;
			}
		}
    }
}
