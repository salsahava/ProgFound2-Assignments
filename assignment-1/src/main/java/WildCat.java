/*
 Author	: Salsabila Hava Qabita
 NPM	: 1706979461
 Kelas	: C
 GitLab Account	: salsahava
 */

public class WildCat {
	// Instance variables
    String name;
    double weight; // In kilograms
    double length; // In centimeters

	// Constructor
    public WildCat(String name, double weight, double length) {
		this.name = name;
		this.weight = weight;
		this.length = length;
    }
	
	// Menghitung mass index dari cat
    public double computeMassIndex() 
	{
		double bmi = weight / ((length * length) * 0.0001);
        return bmi;
    }
}
