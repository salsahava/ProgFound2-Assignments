/*
 Author	: Salsabila Hava Qabita
 NPM	: 1706979461
 Kelas	: C
 GitLab Account	: salsahava
 */

public class TrainCar {
	
    public static final double EMPTY_WEIGHT = 20; // In kilograms
	
	private WildCat cat;
	private TrainCar next;

	// Constructor overload
    public TrainCar(WildCat cat) {
		this.cat = cat;
    }

    public TrainCar(WildCat cat, TrainCar next) {
		this.cat = cat;
		this.next = next;
    }
	
	// Menghitung total berat dari kereta
    public double computeTotalWeight() 
	{
		double berat = EMPTY_WEIGHT + cat.weight;
		double total = 0.0;
		
		if (this.next == null) return berat;
		else{
			total += berat + next.computeTotalWeight();
			return total;
		}
    }
	
	// Menghitung total mass index dari kucing-kucing yang ada di kereta
    public double computeTotalMassIndex() 
	{
		double mass = cat.computeMassIndex();
		double total = 0.0;
		
		if (this.next == null) return mass;
		else{
			total += mass + next.computeTotalMassIndex();
			return total;
		}
    }

	// Print nama kucing yang ada dalam gerbong tersebut
    public void printCar() 
	{
		if (this.next == null){
			System.out.println("(" + cat.name + ")");
		}
		else{
			System.out.print("(" + cat.name + ")" + "--");
			next.printCar();
		}
    }
}
