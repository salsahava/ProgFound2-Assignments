/*
 Author	: Salsabila Hava Qabita
 NPM	: 1706979461
 Kelas	: C
 GitLab Account	: salsahava
 */
 
package animals;

import java.util.Scanner;

public class Hamster extends Animals{

    public Hamster (String name, int length){
        super(name, length, "hamster");
    }
    
    public void gnaw()
    {
        System.out.println(String.format("%s makes a voice: ngkkrit.. ngkkrrriiit", name));
    }
    
    public void run()
    {
        System.out.println(String.format("%s makes a voice: trrr... trrr...", name));
    }
    
    public void choose()
    {
        Scanner input = new Scanner(System.in);
        
        System.out.println("1: See it gnawing 2: Order to run in the hamster wheel");
        
        String num = input.nextLine();
        
        if (num.equals("1")) this.gnaw();
        else if (num.equals("2")) this.run();
        else System.out.print("You do nothing...");
    }
}