/*
 Author	: Salsabila Hava Qabita
 NPM	: 1706979461
 Kelas	: C
 GitLab Account	: salsahava
 */

package animals;

import java.util.Scanner;
import java.util.Random;

public class Cat extends Animals{
    
    public Cat (String name, int length){
        super(name, length, "cat");
    }
    
    public void brushFur()
    {
        System.out.printf("Time to clean %s's fur\n%s makes a voice: Nyaaan...", name, name);
    }
    
    public void cuddle()
    {
        String[] voices = {"Miaaw..", "Purrr..", "Mwaw!", "Mraaawr!"};
        Random r = new Random();
        
        System.out.printf("%s makes a voice: %s\n", name, voices[r.nextInt(4)]);
    }
    
    public void choose()
    {
        Scanner input = new Scanner(System.in);
        
        System.out.println("1: Brush the fur 2: Cuddle");
        
        String num = input.nextLine();
        
        if (num.equals("1")) this.brushFur();
        else if (num.equals("2")) this.cuddle();
        else System.out.print("You do nothing...");
    }
}