/*
 Author	: Salsabila Hava Qabita
 NPM	: 1706979461
 Kelas	: C
 GitLab Account	: salsahava
 */

package animals;
 
public class Animals{

    protected String name, jenis;
    protected int length;
    
    public Animals(String name, int length, String jenis){
        this.name = name;
        this.length = length;
        this.jenis = jenis;
    }
    
    public String getName(){ 
        return name; 
    }
    
    public int getLen(){ 
        return length;
    }
    
    public String getJenis(){
        return jenis;
    }
}