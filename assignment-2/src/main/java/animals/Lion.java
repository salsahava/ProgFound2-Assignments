/*
 Author	: Salsabila Hava Qabita
 NPM	: 1706979461
 Kelas	: C
 GitLab Account	: salsahava
 */

package animals;

import java.util.Scanner;

public class Lion extends Animals{

    public Lion (String name, int length){
        super(name, length, "lion");
    }
    
    public void hunt()
    {
        System.out.println(String.format("Lion is hunting..\n%s makes a voice: err...", name));
    }
    
    public void brushMane()
    {
        System.out.println(String.format("Clean the lion's mane..\n%s makes a voice: Hauhhmm!", name));
    }
    
    public void disturb()
    {
        System.out.println(String.format("%s makes a voice: HAUHHMM!", name));
    }
    
    public void choose()
    {
        Scanner input = new Scanner(System.in);
        
        System.out.println("1: See it hunting 2: Brush the mane 3: Disturb it");
        
        String num = input.nextLine();
        
        if (num.equals("1")) this.hunt();
        else if (num.equals("2")) this.brushMane();
        else if (num.equals("3")) this.disturb();
        else System.out.print("You do nothing...");
    }
}