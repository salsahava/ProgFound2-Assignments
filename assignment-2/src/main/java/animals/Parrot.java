/*
 Author	: Salsabila Hava Qabita
 NPM	: 1706979461
 Kelas	: C
 GitLab Account	: salsahava
 */

package animals;

import java.util.Scanner;

public class Parrot extends Animals{
    
    Scanner input = new Scanner(System.in);

    public Parrot (String name, int length){
        super(name, length, "parrot");
    }
    
    public void fly()
    {
        System.out.println(String.format("Parrot %s flies!\n%s makes a voice: FLYYYY...", name, name));
    }
    
    public void doNothing()
    {
        System.out.println(String.format("%s says: HM?\n", name));
    }
    
    public void doConvo()
    {
      
      System.out.print("You say: ");
      String kalimat = input.nextLine().toUpperCase();
      
      System.out.print(String.format("%s says: %s\n", name, kalimat));
      
    }
    
    public void choose()
    {
        System.out.println("1: Order to fly 2: Do conversation");
        
        String num = input.nextLine();
        
        if (num.equals("1")) this.fly();
        else if (num.equals("2")) this.doConvo();
        else this.doNothing();
    }
}