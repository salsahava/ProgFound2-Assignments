/*
 Author	: Salsabila Hava Qabita
 NPM	: 1706979461
 Kelas	: C
 GitLab Account	: salsahava
 */

package animals;

import java.util.Scanner;

public class Eagle extends Animals{

    public Eagle (String name, int length){
        super(name, length, "eagle");
    }
    
    public void fly()
    {
        System.out.println(String.format("%s makes a voice: kwaakk...\nYou hurt!", name));
    }
    
    public void choose()
    {
        Scanner input = new Scanner(System.in);
        
        System.out.println("1: Order to fly");
        
        String num = input.nextLine();
        
        if (num.equals("1")) this.fly();
        else System.out.print("You do nothing...\n");
    }
}