/*
 Author	: Salsabila Hava Qabita
 NPM	: 1706979461
 Kelas	: C
 GitLab Account	: salsahava
 */

import java.util.Scanner;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import animals.*;
import cage.*;

public class MainOffice {

    public static Scanner input = new Scanner(System.in);

    public static ArrayList<Animals>
            cats = new ArrayList<Animals>(),
            lions = new ArrayList<Animals>(),
            eagles = new ArrayList<Animals>(),
            parrots = new ArrayList<Animals>(),
            hamsters = new ArrayList<Animals>();

    public static Cages catCages, lionCages, eagleCages, parrotCages, hamsterCages;
    public static Map<String, ArrayList<Animals>> binatangs = createMap();

    public static void main(String[] args) {
        System.out.println("Welcome to Javari Park!\nInput the number of animals");

        processInput();

        System.out.println("Animals has been successfully recorded!\n");

        processCages();

        String inputan, jenis;

        String[] names = {"cat", "eagle", "hamster", "parrot", "lion"};

        while (true) {
            System.out.println("Which animal you want to visit?");
            System.out.println("(1: Cat, 2: Eagle, 3: Hamster, 4; Parrot, 5: Lion, 99: Exit)");
            inputan = input.nextLine();

            if (inputan.equals("99")) break;
            else if (Integer.parseInt(inputan) > 5) {
                System.out.println("You do nothing...\n");
                continue;
            }

            jenis = names[Integer.parseInt(inputan) - 1];

            System.out.printf("Mention the name of %s you want to visit: ", jenis);
            inputan = input.nextLine();

            boolean check = false;
            Animals binatang = null;

            for (Animals binatang2 : binatangs.get(jenis)) {
                if (binatang2.getName().equals(inputan)) {
                    check = true;
                    binatang = binatang2;
                    break;
                }
            }

            if (check) {
                System.out.printf("You are visiting %s (%s) now, what would you like to do?\n", inputan, jenis);
                if (jenis.equals("cat")) Cat.class.cast(binatang).choose();
                else if (jenis.equals("eagle")) Eagle.class.cast(binatang).choose();
                else if (jenis.equals("hamster")) Hamster.class.cast(binatang).choose();
                else if (jenis.equals("parrot")) Parrot.class.cast(binatang).choose();
                else if (jenis.equals("lion")) Lion.class.cast(binatang).choose();
            } else System.out.printf("There is no %s with that name! ", jenis);

            System.out.println("Back to the office!\n");
        }
    }

    public static void processInput() {
        String[] animType = {"cat", "lion", "eagle", "parrot", "hamster"};
        int numOfAnim;

        for (String jenis : animType) {
            System.out.printf("%s: ", jenis);
            numOfAnim = Integer.parseInt(input.nextLine());

            if (numOfAnim > 0) {
                System.out.println(String.format("Provide the information of %s(s):", jenis));
                String[] inputan = input.nextLine().split(",");

                for (int i = 0; i < numOfAnim; i++) {
                    String[] info = inputan[i].split("\\|");
                    binatangs.get(jenis).add(createAnim(jenis, info[0], Integer.parseInt(info[1])));
                }

            }

        }
    }

    public static void processCages() {
        if (!cats.isEmpty()) {
            catCages = new Cages(cats, "indoor");
        }
        if (!lions.isEmpty()) {
            lionCages = new Cages(lions, "outdoor");
        }
        if (!eagles.isEmpty()) {
            eagleCages = new Cages(eagles, "outdoor");
        }
        if (!parrots.isEmpty()) {
            parrotCages = new Cages(parrots, "indoor");
        }
        if (!hamsters.isEmpty()) {
            hamsterCages = new Cages(hamsters, "indoor");
        }

        Cages.arrangeCages();
    }

    public static Animals createAnim(String jenis, String name, int length) {
        if (jenis.equals("cat")) return new Cat(name, length);
        if (jenis.equals("lion")) return new Lion(name, length);
        if (jenis.equals("eagle")) return new Eagle(name, length);
        if (jenis.equals("parrot")) return new Parrot(name, length);
        if (jenis.equals("hamster")) return new Hamster(name, length);

        return null;
    }

    public static Map<String, ArrayList<Animals>> createMap() {
        Map<String, ArrayList<Animals>> map = new HashMap<String, ArrayList<Animals>>();
        map.put("cat", cats);
        map.put("lion", lions);
        map.put("eagle", eagles);
        map.put("parrot", parrots);
        map.put("hamster", hamsters);

        return map;
    }
}