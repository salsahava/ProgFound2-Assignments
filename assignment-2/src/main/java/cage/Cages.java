package cage;

import animals.*;

import java.util.*;

public class Cages {

    private static ArrayList<Cages> allKandang = new ArrayList<Cages>();

    private ArrayList<Animals> animals;
    private String loc, jenis;
    private Level[] level = new Level[3];

    public Cages(ArrayList<Animals> animals, String loc) {
        this.animals = animals;
        this.loc = loc;
        this.jenis = animals.get(0).getJenis();

        for (int i = 0; i < 3; i++)
            level[i] = new Level();

        int index = 0;
        int counter = 0;

        int max = (int) (animals.size() / 3);
        max = (animals.size() % 3 == 2 && max != 0) ? max + 1 : max;
        max = (max == 0) ? 1 : max;

        for (int i = 0; i < animals.size(); i++) {

            Animals hewan = animals.get(i);

            level[index].add(new Cage(hewan, loc));

            counter++;

            if (counter >= max) {
                index = (index + 1) % 3;
                counter = 0;
            }

            if ((i == animals.size() - 2) && (animals.size() % max == 1))
                index = 2;
        }

        allKandang.add(this);
    }

    public static void arrangeCages() {

        Map<String, Integer> data = new LinkedHashMap<String, Integer>();
        data.put("cat", 0);
        data.put("lion", 0);
        data.put("eagle", 0);
        data.put("parrot", 0);
        data.put("hamster", 0);

        System.out.println("=============================================");
        System.out.println("Cage arrangement: ");

        for (Cages currentCages : allKandang) {

            data.put(currentCages.getJenis(), currentCages.getAnimals().size());

            System.out.println("location: " + currentCages.getLoc());

            currentCages.getInfo();

            for (Level level : currentCages.level) {
                level.reverse();
            }

            Level temp1 = currentCages.level[2], temp2;
            for (int l = 0; l < 3; l++) {
                temp2 = currentCages.level[l];
                currentCages.level[l] = temp1;
                temp1 = temp2;
            }

            System.out.println("After rearrangement...");

            currentCages.getInfo();
        }

        System.out.println("NUMBERS OF ANIMALS:");
        for (String key : data.keySet()) {
            System.out.println(key + ":" + data.get(key));
        }
        System.out.println("\n=============================================");

    }

    public String getLoc() {
        return this.loc;
    }

    public String getJenis() {
        return this.jenis;
    }

    public ArrayList<Animals> getAnimals() {
        return this.animals;
    }


    public void getInfo() {
        for (int i = 2; i >= 0; i--) {
            System.out.printf("level %d: ", (i + 1));

            if (!this.level[i].isEmpty()) {
                for (Cage cage : this.level[i].getCages())
                    System.out.print(cage);
            }
            System.out.println();
        }
        System.out.println();
    }
}