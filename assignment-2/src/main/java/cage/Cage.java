/*
 Author	: Salsabila Hava Qabita
 NPM	: 1706979461
 Kelas	: C
 GitLab Account	: salsahava
 */

package cage;

import animals.*;

public class Cage {

    private Animals animal;
    private String type = "C";

    public Cage(Animals animal, String loc) {
        this.animal = animal;

        if (loc.equals("indoor")) {
            this.type = (animal.getLen() < 60) ? "B" : this.type;
            this.type = (animal.getLen() < 45) ? "A" : this.type;
        } else {
            this.type = (animal.getLen() < 90) ? "B" : this.type;
            this.type = (animal.getLen() < 75) ? "A" : this.type;
        }
    }

    public String toString() {
        return (String.format("%s (%d - %s)", animal.getName(), animal.getLen(), type));
    }

    public boolean isValid() {
        if (animal == null) return false;
        else return true;
    }

}