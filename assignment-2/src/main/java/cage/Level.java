/*
 Author	: Salsabila Hava Qabita
 NPM	: 1706979461
 Kelas	: C
 GitLab Account	: salsahava
 */

package cage;

import java.util.ArrayList;

public class Level {

    private ArrayList<Cage> kandang;

    public Level() {
        this.kandang = new ArrayList<Cage>();
    }

    public ArrayList<Cage> getCages() {
        return kandang;
    }

    public void add(Cage cage) {
        kandang.add(cage);
    }

    public boolean isEmpty() {
        return kandang.isEmpty();
    }

    public void reverse() {
        ArrayList<Cage> levelBaru = new ArrayList<Cage>();

        for (int i = kandang.size() - 1; i >= 0; i--) {
            levelBaru.add(kandang.get(i));
        }

        kandang = levelBaru;
    }
}